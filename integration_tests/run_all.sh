#!/bin/bash

# start Postgres & DB API client
docker-compose create
docker-compose start

sleep 30
docker ps -a

# add user
docker exec orchestrator-db_api_dev-1 bash -c "python3 app/create_api_user.py -u admin -p test"

# API is accessed locally with docker:port
export URL_FROM_LOCAL="http://${API_HOST}:${PORT}"
# API is accessed from inside the docker container by 0.0.0.0:port
export URL_HOST_FROM_CT="http://${API_IP}:${PORT}"
echo API url ${URL_FROM_LOCAL}

# auth
RESPONSE=`curl -X POST "${URL_FROM_LOCAL}/v1/auth/token" \
--header "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "username=admin" \
--data-urlencode "password=test" \
--data-urlencode "scope=me read:associate read:dejup write:dejup read:osm write:osm write:associate read:dejup_locations write:dejup_locations read:google write:google read:edenred write:edenred read:edenred_locations write:edenred_locations"`

export TOKEN=`echo "$RESPONSE" | awk -F'"' '{print $4}'`

echo TOKEN ${TOKEN}

# add Dejup index
curl -X POST "${URL_FROM_LOCAL}/v1/locations/dejup" \
--header "Authorization: Bearer ${TOKEN}" \
--header "Content-Type: application/json" \
--data '{"zipcode":91190}'

echo Dejup index ok

# scrap site
docker run -e URL="${URL_HOST_FROM_CT}" --network="host" registry.gitlab.com/geoplaces/scrapper-updejeuner:${SCRAP_TAG}
echo Dejup scrap ok

# update with OSM
docker run -e TOKEN=${TOKEN} -e URL="${URL_HOST_FROM_CT}" -e OSM_EMAIL="${EMAIL}" --network="host" registry.gitlab.com/geoplaces/osm_client:${OSM_TAG} python3 main.py --osm --limit=10
echo Dejup OSM ok

# Add Edenred index
curl -X POST "${URL_FROM_LOCAL}/v1/locations/edenred" \
--header "Authorization: Bearer ${TOKEN}" \
--header "Content-Type: application/json" \
--data '{"lat": 48.858742,"long": 2.339270,"radius": 0.5}'

echo Edenred index ok

# scrap site
docker run -e URL="${URL_HOST_FROM_CT}" --network="host" registry.gitlab.com/geoplaces/scrapper-updejeuner:${SCRAP_TAG} python3 main.py --source=edenred
echo Edenred scrap ok

# update with OSM
docker run -e TOKEN=${TOKEN} -e URL="${URL_HOST_FROM_CT}" -e OSM_EMAIL="${EMAIL}" --network="host" registry.gitlab.com/geoplaces/osm_client:${OSM_TAG} python3 main.py --osm --source=edenred --limit=10
echo Edenred OSM ok

# Test FE client
apk add python3 py3-pip
pip install -r requirements.txt
python3 test_web.py
if [[ $? != 0 ]]
then
    exit 1
fi
