from sys import exit
from os import getenv
import requests

def check(response):
    if response.status_code != 200:
        print(f'Error received: {response.status_code} {response.json()}')
        exit(1)

def main():
    URL = getenv('URL_FROM_LOCAL')
    # main page
    print('Test main')
    check(requests.get(URL))

    # API hook for JS
    print('Test dejup hook')
    response = requests.get(
            f'{URL}/v1/associations/dejup/within_coords?n=52.133488040771496&s=40.78054143186033&w=-5.800781250000001&e=14.23828125')
    check(response)
    if response.json()['total'] == 0:
        exit(1)

    print('Test dejup hook native')
    response = requests.get(
        f'{URL}/v1/dejup/places_coords?n=52.133488040771496&s=40.78054143186033&w=-5.800781250000001&e=14.23828125')
    check(response)
    if response.json()['total'] == 0:
        exit(1)

    # API hook for JS
    print('Test edenred hook')
    response = requests.get(
        f'{URL}/v1/associations/edenred/within_coords?n=52.133488040771496&s=40.78054143186033&w=-5.800781250000001&e=14.23828125')
    check(response)
    if response.json()['total'] == 0:
        print('Empty response')
        exit(1)

    print('Test edenred hook native')
    response = requests.get(
        f'{URL}/v1/edenred/places_coords?n=52.133488040771496&s=40.78054143186033&w=-5.800781250000001&e=14.23828125')
    check(response)
    if response.json()['total'] == 0:
        print('Empty response')
        exit(1)


if __name__ == "__main__":
    main()