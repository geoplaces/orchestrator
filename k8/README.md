# Kubernetes deployment

## Setup

### Database API

1. Postgres database is expected to be setup with created user and empy database.
Below they are represented with env vars like `${DB_USER}` and so on.

### Logs 
Logs are stored with Persistent Volume. To create one update pv_logs.yaml with your node name and:
```bash
kubectl apply -f pv_logs.yaml
```

if the logs are not needed remove the `volumeMounts` clause from db_api_deployment.yaml

### Database API

1. Create secrets for DB API with optional sentry dsn.

    ```bash
    kubectl create secret generic db-api-creds \
    --from-literal=algo=HS256 \
    --from-literal=secret=`openssl rand -hex 32` \
    --from-literal=db_url=postgresql://${DB_USER}:${DB_PASSW}@${DB_HOST}:5432/${DB_NAME} \
    --from-literal=sentry_dsn=""
    ```
2. Create DB API deployment & service

    ```bash
    kubectl apply -f db_api_deployment.yaml
    ```

    Service exposes port 30001 as a NodePort.

3. Upgrade DB model to the latest version

    ```bash
    export POD_NAME=`kubectl get pods -o name | grep db-api-deployment | head -n1`
    kubectl exec ${POD_NAME} -- bash -c "cd app; alembic upgrade head"
    ```

4. Create API clients creds:

    ```bash
    export API_USER="admin"
    export API_PASSW="test"
    kubectl exec ${POD_NAME} -- bash -c "python3 app/create_api_user.py -u ${API_USER} -p ${API_PASSW}"
    ```


### API clients

1. Create secrets for DB API access
    ```bash
    kubectl create secret generic db-api-client-creds \
    --from-literal=client=${API_USER} \
    --from-literal=passw=${API_PASSW}
    ```

2. Create cronjob for scrapping addresses

    Optional monitoring creds:
    ```bash
    kubectl create secret generic scrapper-creds \
    --from-literal=sentry_dsn=""
    --from-literal=heart_beat_url=""
    ```
    
    Service deployment:
    ```bash
    kubectl apply -f scrapper_cron.yaml
    ```

3. Create cronjob for update the addresses with accurate location.
If Google Maps client will be used Google API key has to be set in secrets:

    Optional monitoring creds:
    ```bash
    kubectl create secret generic map-updater-creds \
    --from-literal=sentry_dsn=""
    --from-literal=heart_beat_url=""
    ```

    Optional Google API key
    ```bash
    kubectl create secret generic google-client-secret \
    --from-literal=google-api-key=some_key
    ```

    Service deployment:
    ```bash
    kubectl apply -f map_update_cron.yaml
    ```
