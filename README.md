# GeoPlaces

Scrap addresses from text, complement them with accurate geolocation 
and serve a map with markers to web and phone app. That's it!

![Screenshots](img/screen.png)

GeoPlaces was made to help with positioning on map when only address information is available
and/or positioning is very poor. 

The workflow is the following:
1. The [scrapper](https://gitlab.com/geoplaces/scrapper-updejeuner) extracts the text address from the website/list
2. [OSM/Gmaps client](https://gitlab.com/geoplaces/osm_client) searches for the location with the given address
3. The [DB API](https://gitlab.com/geoplaces/db_api) provides the interface between modules and serves a simple web page with map and markers

The design was made highly modular, so that clients (scraper/map search engine) 
can be easily added/replaced.

![App scheme](img/scheme.png)

# Setup & run

Instructions to run each package are written in their repositories. 
E.g.the core module DB API docs are [here](https://gitlab.com/geoplaces/db_api)

The instructions for Kubernetes setup are in the dedicated [k8 folder](k8/README.md)
